Rails.application.routes.draw do
  resources :appointments
  get 'home/index'
 root 'home#index'
 get 'staff' => 'home#staff'

  resources :britannia_webs
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
