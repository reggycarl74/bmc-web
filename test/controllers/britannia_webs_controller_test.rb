require 'test_helper'

class BritanniaWebsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @britannia_web = britannia_webs(:one)
  end

  test "should get index" do
    get britannia_webs_url
    assert_response :success
  end

  test "should get new" do
    get new_britannia_web_url
    assert_response :success
  end

  test "should create britannia_web" do
    assert_difference('BritanniaWeb.count') do
      post britannia_webs_url, params: { britannia_web: { appointment_date: @britannia_web.appointment_date, appointment_time: @britannia_web.appointment_time, email: @britannia_web.email, fullname: @britannia_web.fullname, phone_number: @britannia_web.phone_number } }
    end

    assert_redirected_to britannia_web_url(BritanniaWeb.last)
  end

  test "should show britannia_web" do
    get britannia_web_url(@britannia_web)
    assert_response :success
  end

  test "should get edit" do
    get edit_britannia_web_url(@britannia_web)
    assert_response :success
  end

  test "should update britannia_web" do
    patch britannia_web_url(@britannia_web), params: { britannia_web: { appointment_date: @britannia_web.appointment_date, appointment_time: @britannia_web.appointment_time, email: @britannia_web.email, fullname: @britannia_web.fullname, phone_number: @britannia_web.phone_number } }
    assert_redirected_to britannia_web_url(@britannia_web)
  end

  test "should destroy britannia_web" do
    assert_difference('BritanniaWeb.count', -1) do
      delete britannia_web_url(@britannia_web)
    end

    assert_redirected_to britannia_webs_url
  end
end
