class BritanniaWebsController < ApplicationController
  before_action :set_britannia_web, only: [:show, :edit, :update, :destroy]

  # GET /britannia_webs
  # GET /britannia_webs.json
  def index
    @britannia_webs = BritanniaWeb.all
  end

  # GET /britannia_webs/1
  # GET /britannia_webs/1.json
  def show
  end

  # GET /britannia_webs/new
  def new
    @britannia_web = BritanniaWeb.new
  end

  # GET /britannia_webs/1/edit
  def edit
  end

  # POST /britannia_webs
  # POST /britannia_webs.json
  def create
    @britannia_web = BritanniaWeb.new(britannia_web_params)

    respond_to do |format|
      if @britannia_web.save
        format.html { redirect_to @britannia_web, notice: 'Britannia web was successfully created.' }
        format.json { render :show, status: :created, location: @britannia_web }
      else
        format.html { render :new }
        format.json { render json: @britannia_web.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /britannia_webs/1
  # PATCH/PUT /britannia_webs/1.json
  def update
    respond_to do |format|
      if @britannia_web.update(britannia_web_params)
        format.html { redirect_to @britannia_web, notice: 'Britannia web was successfully updated.' }
        format.json { render :show, status: :ok, location: @britannia_web }
      else
        format.html { render :edit }
        format.json { render json: @britannia_web.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /britannia_webs/1
  # DELETE /britannia_webs/1.json
  def destroy
    @britannia_web.destroy
    respond_to do |format|
      format.html { redirect_to britannia_webs_url, notice: 'Britannia web was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_britannia_web
      @britannia_web = BritanniaWeb.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def britannia_web_params
      params.require(:britannia_web).permit(:fullname, :email, :phone_number, :appointment_date, :appointment_time)
    end
end
