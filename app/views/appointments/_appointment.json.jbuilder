json.extract! appointment, :id, :fullname, :email, :phone_number, :appointment_date, :appointment_time, :select_department, :created_at, :updated_at
json.url appointment_url(appointment, format: :json)
