json.extract! britannia_web, :id, :fullname, :email, :phone_number, :appointment_date, :appointment_time, :created_at, :updated_at
json.url britannia_web_url(britannia_web, format: :json)
