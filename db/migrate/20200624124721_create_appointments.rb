class CreateAppointments < ActiveRecord::Migration[5.1]
  def change
    create_table :appointments do |t|
      t.string :fullname
      t.string :email
      t.integer :phone_number
      t.datetime :appointment_date
      t.datetime :appointment_time
      t.string :select_department

      t.timestamps
    end
  end
end
